--EXAMPLE IF STATEMENT
DECLARE 
   a number(2) := 10; 
BEGIN 
   a:= 10; 
  -- check the boolean condition using if statement  
   IF( a < 20 ) THEN 
      -- if condition is true then print the following   
      dbms_output.put_line('a is less than 20 ' ); 
   END IF; 
   dbms_output.put_line('value of a is : ' || a); 
END; 
/

--EXAMPLE IF - ELSE
DECLARE 
   a number(3) := 100; 
BEGIN 
   -- check the boolean condition using if statement  
   IF( a < 20 ) THEN 
      -- if condition is true then print the following   
      dbms_output.put_line('a is less than 20 ' ); 
   ELSE 
      dbms_output.put_line('a is not less than 20 ' ); 
   END IF; 
   dbms_output.put_line('value of a is : ' || a); 
END; 
/

--EXAMPLE IF WITH A TABLE
DECLARE 
   c_id customers.id%type := 2; 
   c_sal  customers.salary%type; 
BEGIN 
   SELECT  salary  
   INTO  c_sal 
   FROM customers 
   WHERE id = c_id; 
   IF (c_sal <= 2000) THEN 
      UPDATE customers  
      SET salary =  salary + 1000 
         WHERE id = c_id; 
      dbms_output.put_line ('Salary updated'); 
   END IF; 
END; 
/

--EXAMPLE WITH A TABLE
DECLARE 
    P_ID customers.id%type := 2;
    P_SAL customers.salary%type;
begin
    select salary into P_SAL from customers WHERE ID = p_id;
    
    IF p_sal <= 2000 THEN
          DBMS_OUTPUT.PUT_LINE('SALARY LESS TO 2000' || ' SALARY IS '||P_SAL);
          UPDATE CUSTOMERS
          SET SALARY = SALARY + 500
          WHERE ID = P_ID;
          
              select salary into P_SAL from customers WHERE ID = p_id;

          DBMS_OUTPUT.PUT_LINE('WE UPDATE YOUR SALARY, NOW YOUR SALARY IS: '|| P_SAL);
    ELSE
          DBMS_OUTPUT.PUT_LINE('SALARY IS NOT LESS TO 2000' || ' SALARY IS '||P_SAL);
    END IF;
end;
/

--switch or case
DECLARE 
   grade char(1) := 'A'; 
BEGIN 
   CASE grade 
      when 'A' then dbms_output.put_line('Excellent'); 
      when 'B' then dbms_output.put_line('Very good'); 
      when 'C' then dbms_output.put_line('Well done'); 
      when 'D' then dbms_output.put_line('You passed'); 
      when 'F' then dbms_output.put_line('Better try again'); 
      else dbms_output.put_line('No such grade'); 
   END CASE; 
END; 
/
--switch or case(another sintaxis)
DECLARE 
   grade char(1) := 'B'; 
BEGIN 
   case  
      when grade = 'A' then dbms_output.put_line('Excellent'); 
      when grade = 'B' then dbms_output.put_line('Very good'); 
      when grade = 'C' then dbms_output.put_line('Well done'); 
      when grade = 'D' then dbms_output.put_line('You passed'); 
      when grade = 'F' then dbms_output.put_line('Better try again'); 
      else dbms_output.put_line('No such grade'); 
   end case; 
END; 
/

