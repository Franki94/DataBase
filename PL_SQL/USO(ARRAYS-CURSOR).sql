--set serveroutput on
--USO VARRAY
DECLARE 
   type namesarray IS VARRAY(5) OF VARCHAR2(10);
   type grades IS VARRAY(5) OF INTEGER;
   names namesarray;
   marks grades;
   total integer;
BEGIN 
   names := namesarray('Kavita', 'Pritam', 'Ayan', 'Rishav', 'Aziz'); 
   marks:= grades(98, 97, 78, 87, 92); 
   total := names.count; 
   dbms_output.put_line('Total '|| total || ' Students'); 
   FOR i in 1 .. total LOOP 
      dbms_output.put_line('Student: ' || names(i) || ' 
      Marks: ' || marks(i)); 
   END LOOP; 
END; 
/

--LLENANDO UN VARRAY
DECLARE 
   type namesarray IS VARRAY(5) OF VARCHAR2(10); 
   type grades IS VARRAY(5) OF INTEGER; 
   names namesarray:=namesarray(); 
   marks grades:=grades(); 
   total integer; 
BEGIN 
   total := 5; 
declare cuenta integer:=0;

    BEGIN    
    FOR i IN 1..total LOOP
     cuenta := cuenta + 1;
        names.extend;
         names(i) := 'lala';
         marks.extend;
         marks(i) := cuenta +  5;
    END LOOP;
    END; 
    
    FOR i in 1 .. total LOOP 
      dbms_output.put_line('Student: ' || names(i) || ' 
      Marks: ' || marks(i)); 
   END LOOP; 
END; 
/

--LENANDO UN VARRAY CON UNA TABLA
DECLARE 
   CURSOR c_customers is SELECT  name FROM customers; 
   type c_list is varray (6) of customerS.Name%type; 
   name_list c_list := c_list(); 
   counter integer :=0; 
   
   CURSOR d_customers is SELECT CUSTOMERS.SALARY FROM customers; 
   type d_list is varray (6) of customerS.salary%type;
   salario_list d_list := d_list(); 

BEGIN 
    --PARECIDO A UN FOREACH
    begin
        FOR n IN c_customers LOOP 
            counter := counter + 1; 
            name_list.extend; 
            name_list(counter)  := n.name; 
            dbms_output.put_line('Customer('||counter ||'):'||name_list(counter)); 
        END LOOP; 
   end;
   begin
        counter :=0;
        for n in d_customers loop
            counter := counter +1;
            salario_list.extend;
            salario_list(counter) := n.salary;
            dbms_output.put_line('Customer('||counter ||'):'||salario_list(counter)); 
        end loop;
   end;
END; 
/ 