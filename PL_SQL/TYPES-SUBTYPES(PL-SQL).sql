/*DECLARE 
   SUBTYPE name IS char(20); 
   SUBTYPE message IS varchar2(100);
   salutation name;
   greetings message;
   greetings varchar(100);
   salutation varchar(199);
*/
/*   
BEGIN 
   salutation := 'Reader '; 
   greetings := 'Welcome to the World of PL/SQL'; 
   dbms_output.put_line('Hello ' || salutation || greetings); 
END;*/
CREATE TABLE CUSTOMERS( 
   ID   INT NOT NULL, 
   NAME VARCHAR (20) NOT NULL, 
   AGE INT NOT NULL, 
   ADDRESS CHAR (25), 
   SALARY   DECIMAL (18, 2),        
   PRIMARY KEY (ID) 
);  
INSERT INTO CUSTOMERS (ID,NAME,AGE,ADDRESS,SALARY) 
VALUES (1, 'Ramesh', 32, 'Ahmedabad', 2000.00 );  

select * from customers order by id desc;

DECLARE 
   c_id customers.id%type := 1;
   c_name  customers.Name%type;
   c_addr customers.address%type;
   c_sal  customers.salary%type;
BEGIN 
   SELECT name, address, salary INTO c_name, c_addr, c_sal 
   FROM customers 
   WHERE id = c_id;  
   dbms_output.put_line 
   ('Customer ' ||c_name || ' from ' || c_addr || ' earns ' || c_sal); 
END; 
/ 

set serveroutput on
