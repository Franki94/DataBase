Create procedure SP_INS_EMPLEADO
(@P_Nombre varchar(25),
@P_Apellido varchar(25),
@P_Cargo INT,
@P_Horario int
)
as
begin
declare @cuenta int;

		SELECT @cuenta=COUNT(1) FROM EMPLEADOS;
		set @cuenta = @cuenta + 1;
	begin transaction
		insert into EMPLEADOS(IDEMPLEADO, NOMBRE,APELLIDO,IDCARGO,IDHORARIO)
		values(@cuenta,@P_Nombre, @P_Apellido, @P_Cargo, @P_Horario);
	commit transaction

	select * from EMPLEADOS where IDEMPLEADO = @cuenta;
end